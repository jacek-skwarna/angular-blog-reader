import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { catchError, map, concatMap, tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import Comment from '../helpers/comment';
import errorHandler from '../helpers/error-handler';

const API_URLS = {
  getCommentsByPostId: `${environment.apiBaseUrl}/posts/$post_ID/replies?number=1&fields=ID,author,date,content,post`
};

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  private static generateCommentRequestUrl(baseUrl: string, postId: number): string {
    return API_URLS.getCommentsByPostId.replace('$post_ID', `${postId}`);
  }

  getRecentCommentsByPostsIds(postsIds: number[]): Observable<Comment> {
    return from(postsIds).pipe(
      concatMap(postId => <Observable<Comment>> this.http
        .get<any>(CommentsService.generateCommentRequestUrl(API_URLS.getCommentsByPostId, postId))
        .pipe(
          map(res => res.comments ? res.comments[0] : undefined),
          catchError(errorHandler)
        )
      )
    );
  }
}
