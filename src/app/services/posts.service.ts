import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import Post from '../helpers/post';
import errorHandler from '../helpers/error-handler';

const API_URLS = {
  getPosts: `${environment.apiBaseUrl}/posts?number=3&fields=ID,content,author,date`
};

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  static formatPost(postData: Post): Post {
    const { ID, author, content } = postData;

    return new Post({ ID, author, content });
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<any>(API_URLS.getPosts)
      .pipe(
        map(res => res.posts ? res.posts : []),
        catchError(errorHandler)
      );
  }
}
