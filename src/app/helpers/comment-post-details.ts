export default class CommentPostDetails {
  ID: number;

  constructor(postDetails: Object = {}) {
    Object.assign(this, postDetails);
  }
}
