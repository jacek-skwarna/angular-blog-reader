import CommentPostDetails from './comment-post-details';
import Author from './author';

export default class Comment {
  ID: number;
  author: Author;
  date: string;
  content: string;
  post: CommentPostDetails;

  constructor(commentData: Object = {}) {
    Object.assign(this, commentData);
  }
}
