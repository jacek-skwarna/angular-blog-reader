import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

const errorHandler: any = (error: HttpErrorResponse) => {
  // ToDo: Add custom error logs
  return throwError(error);
};

export default errorHandler;
