import Author from './author';

export default class Post {
  ID: number;
  author: Author;
  content: string;
  date: string;

  constructor(postData: Object = {}) {
    Object.assign(this, postData);
  }
}
