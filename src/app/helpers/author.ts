export default class Author {
  ID: number;
  name: string;

  constructor(authorDetails: Object = {}) {
    Object.assign(this, authorDetails);
  }
}
