import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { CommentsService } from '../../services/comments.service';
import Post from '../../helpers/post';
import Comment from '../../helpers/comment';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {

  constructor(private postsService: PostsService, private commentsService: CommentsService) { }

  postsListHeader = 'Posts';
  posts: Post[] = [];
  comments: Map<number, Comment> = new Map();
  selectedPost;
  loadingPosts: boolean;

  selectPost(post): void {
    this.selectedPost = post;
  }

  private getPostsIds(): number[] {
    return this.posts.map(post => post.ID);
  }

  loadComments(): void {
    const posts = this.posts;

    if (posts.length) {
      this.commentsService.getRecentCommentsByPostsIds(this.getPostsIds())
      .subscribe(
        comment => {
          if (!comment) {
            return;
          }
          this.comments.set(comment.post.ID, comment);
        },
        error => {
          // ToDo: show nice looking notification for user
          console.error(error);
        }
      );
    }
  }

  getPosts(): void {
    this.loadingPosts = true;
    this.postsService.getPosts()
    .subscribe(
      posts => {
        this.posts = posts;
        this.loadingPosts = false;
      },
      error => {
        // ToDo: show nice looking notification for user
        console.error(error);
        this.loadingPosts = false;
      }
    );
  }

  ngOnInit() {
    this.getPosts();
  }
}
