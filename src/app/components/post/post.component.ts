import { Component, OnInit, Input } from '@angular/core';
import Post from '../../helpers/post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  constructor() { }

  @Input() post: Post;
  @Input() selectedPost: Post;
  @Input() relatedComment: Comment;

  ngOnInit() {
  }
}
