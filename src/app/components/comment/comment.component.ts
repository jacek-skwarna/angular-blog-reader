import { Component, OnInit, Input } from '@angular/core';
import Comment from '../../helpers/comment';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  constructor() { }

  @Input() commentDetails: Comment;

  ngOnInit() {
  }

}
