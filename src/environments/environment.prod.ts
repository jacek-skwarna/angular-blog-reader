export const environment = {
  production: true,
  apiBaseUrl: 'https://public-api.wordpress.com/rest/v1.1/sites/en.blog.wordpress.com'
};
